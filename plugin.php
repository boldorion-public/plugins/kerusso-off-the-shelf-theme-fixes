<?php
/*
Plugin Name: Kerusso Off The Shelf theme fixes
Plugin URI: https://www.boldorion.com
Description: The Off The Shelf theme (and subsequent plugins) are no longer maintained. This plugin fixes issues caused by jQuery deprecations caused in WordPress versions 5.6 and upwards.
Version: 1.0.9
Author: Duncan Platt
Author URI: https://www.boldorion.com
License: Proprietary
Comissioned by: Chris Rainey
*/

/*
 * Let's ensure we can get updates for this
 */

require 'updater/plugin-update-checker.php';

$myUpdateChecker = new Puc_v4p10_Vcs_PluginUpdateChecker(
	new Puc_v4p10_Vcs_GitLabApi('https://gitlab.com/boldorion-public/plugins/kerusso-off-the-shelf-theme-fixes', null, 'plugins'),
	__FILE__,
	'kerusso-off-the-shelf-theme-fixes'
);

$myUpdateChecker->setBranch('master');


/*
 * The Actual Plugin
 */
 
//Let's get rid of broken things.
function fix_deprecated_scripts() {
	$plugindir = plugin_dir_path( __FILE__ );
	//Lets remove 'em
    //$scripts = array("offtheshelf-featherlight", "offtheshelf-swipebox");
		
    wp_dequeue_script( 'offtheshelf-swipebox' );
	wp_deregister_script( 'offtheshelf-swipebox' );
		
    wp_dequeue_script( 'offtheshelf-featherlight' );
	wp_deregister_script( 'offtheshelf-featherlight' );

    wp_dequeue_script( 'offtheshelf-classie' );
	wp_deregister_script( 'offtheshelf-classie' );

    wp_dequeue_script( 'offtheshelf-salvattore' );
	wp_deregister_script( 'offtheshelf-salvattore' );
	
    wp_dequeue_script( 'offtheshelf-smartmenus' );
	wp_deregister_script( 'offtheshelf-smartmenus' );
	
    wp_dequeue_script( 'offtheshelf-flexslider' );
	wp_deregister_script( 'offtheshelf-flexslider' );
	
    wp_dequeue_script( 'offtheshelf-pagescroll2id' );
	wp_deregister_script( 'offtheshelf-pagescroll2id' );
	
    wp_dequeue_script( 'offtheshelf-waypoints' );
	wp_deregister_script( 'offtheshelf-waypoints' );
	
	
	//Let's add fixed ones
	wp_enqueue_script( 'bo-featherlight', 'https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.7.13/featherlight.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-classie', 'https://cdnjs.cloudflare.com/ajax/libs/classie/1.0.1/classie.min.js');
	wp_enqueue_script( 'bo-classie', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.smartmenus/1.0.2/jquery.smartmenus.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/flexslider.min.css');
	wp_enqueue_script( 'bo-swipebox', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.4/js/jquery.swipebox.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-salvattore', 'https://cdnjs.cloudflare.com/ajax/libs/salvattore/1.0.9/salvattore.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-swipebox', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.3.1/jquery.placeholder.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-pagescroll2id', $plugindir .'/js/jquery.PageScroll2id.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-waypoints', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js', array( 'jquery' ));
	wp_enqueue_script( 'bo-sticky', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js', array( 'jquery', 'bo-waypoints' ));
	
}

add_action( 'wp_enqueue_scripts', 'fix_deprecated_scripts', 100 );

//Disable theme updates
define( 'OFFTHESHELF_ENABLE_UPDATES', false );